package com.github.antweb.donkey.file

interface FileTransferListener {
    fun onError() {}
    fun onFinish() {}
}