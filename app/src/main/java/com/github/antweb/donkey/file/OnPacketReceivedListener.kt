package com.github.antweb.donkey.file

import com.github.antweb.donkey.file.protocol.Packet

interface OnPacketReceivedListener {
    fun onPacketReceived(packet: Packet)
}