package com.github.antweb.donkey.file

import android.util.Log
import com.github.antweb.donkey.file.protocol.Packet
import com.github.antweb.donkey.file.protocol.PacketType
import com.github.antweb.donkey.file.protocol.TransferState
import com.github.antweb.donkey.toHex
import java.nio.Buffer
import java.nio.charset.Charset

private const val TAG = "FileTransfer"

class FileTransfer(
    private val service: LowEffortService,
    private val reader: ChunkedReader,
    private var listener: FileTransferListener
) : OnPacketReceivedListener {

    private var currentState = TransferState.IDLE

    init {
        service.addOnPacketReceivedListener(this)
    }

    override fun onPacketReceived(packet: Packet) {
        Log.d(TAG, "Received packet ${packet.type.prefix} ${toHex(packet.payload)}")
        Log.d(TAG, "Received packet ${packet.type.prefix} ${String(packet.payload, Charset.defaultCharset())}")

        when (packet.type) {
            PacketType.START_ACK -> {
                if (currentState != TransferState.START_SENT) {
                    abort()
                } else {
                    currentState = TransferState.READY_TO_SEND
                    sendNext()
                }
            }

            PacketType.CHUNK_ACK -> {
                if (currentState == TransferState.READY_TO_SEND || currentState == TransferState.CHUNK_SENT) {
                    if (verifyCrc(packet)) {
                        sendNext()
                    } else {
                        // TODO: Retry
                        abort()
                    }
                } else {
                    abort()
                }
            }

            PacketType.FINISH_ACK -> {
                if (currentState != TransferState.FINISH_SENT) {
                    abort()
                } else {
                    currentState = TransferState.IDLE
                    listener.onFinish()
                }
            }

            PacketType.ERROR -> {
                // Abort transfer
                peripheralAbort()
                listener.onError()
            }

            PacketType.ERROR_ACK -> {
                // TODO
            }

            else -> {
                abort()
            }
        }
    }

    fun start() {
        if (currentState != TransferState.IDLE) {
            throw IllegalStateException()
        }

        service.sendPacket(
            Packet(
                PacketType.START,
                "/foo.py".toByteArray(Charset.forName("ASCII"))
            )
        )
        currentState = TransferState.START_SENT
    }

    private fun sendNext() {
        if (!reader.isDone()) {
            var status = service.sendPacket(reader.getNext())
            var retryCount = 1
            while (!status && retryCount < 3) {
                status = service.sendPacket(reader.getLast())
                retryCount++
            }
        } else {
            service.sendPacket(
                Packet(
                    PacketType.FINISH,
                    ByteArray(0)
                )
            )
            currentState = TransferState.FINISH_SENT
        }
    }

    private fun verifyCrc(packet: Packet): Boolean {
        // TODO: Verify CRC
        return true
    }

    private fun peripheralAbort() {
        // Peripheral sent error. Only clean up
    }

    fun abort() {
        // We want to abort. Clean up and send ERROR
        service.sendPacket(
            Packet(
                PacketType.ERROR,
                ByteArray(0)
            )
        )
        listener.onError()
    }
}