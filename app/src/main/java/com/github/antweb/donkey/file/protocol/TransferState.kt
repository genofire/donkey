package com.github.antweb.donkey.file.protocol

enum class TransferState {
    IDLE,
    START_SENT,
    READY_TO_SEND,
    CHUNK_SENT,
    FINISH_SENT
}