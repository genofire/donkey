package com.github.antweb.donkey.file.protocol

enum class PacketType(val prefix: Char) {
    START('s'),
    START_ACK('S'),
    CHUNK('c'),
    CHUNK_ACK('C'),
    FINISH('f'),
    FINISH_ACK('F'),
    ERROR('e'),
    ERROR_ACK('E')
}