package com.github.antweb.donkey.file

import android.content.Context
import android.net.Uri
import android.util.Log
import com.github.antweb.donkey.file.protocol.Packet
import com.github.antweb.donkey.file.protocol.PacketType
import java.lang.IllegalStateException
import java.nio.ByteBuffer
import java.nio.charset.Charset

private const val TAG = "ChunkedReader"

class ChunkedReader(
    context: Context,
    uri: Uri,
    private val mtu: Int
) {
    private val iterator: Iterator<Byte>
    private var lastPacket: Packet? = null

    init {
        val inputStream = context.getContentResolver().openInputStream(uri) ?: throw IllegalStateException()
        val bytes = inputStream.readBytes()
        iterator = bytes.iterator()
    }

    private var offset: Int = 0
    private var lastCrc: Long = 0

    fun verifyCrc(crc: Long): Boolean = (lastCrc == crc)

    fun isDone() = !iterator.hasNext()

    fun getLast(): Packet {
        return lastPacket ?: throw IllegalStateException()
    }

    fun getNext(): Packet {
        Log.d(TAG, "Next chunk: $offset")

        if (!iterator.hasNext()) {
            throw IndexOutOfBoundsException()
        }

        // Our packet:
        // 1 byte header + 4 byte offset + chunk
        val maxChunkSize = mtu - 5 - 10

        val buffer = ByteBuffer.allocate(mtu - 11)
        buffer.putInt(offset)

        // TODO: Make less hacky
        var n = 0
        for (i in 0 until maxChunkSize) {
            if (!iterator.hasNext()) {
                break
            }

            buffer.put(iterator.next())
            n++
        }

        var byteArray = buffer.array().sliceArray(0..n)
        offset += n

        val packet = Packet(
            PacketType.CHUNK,
            byteArray
        )

        lastCrc = packet.getCrc()

        lastPacket = packet

        return packet
    }
}