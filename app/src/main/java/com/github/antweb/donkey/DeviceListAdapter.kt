package com.github.antweb.donkey

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class DeviceListAdapter(context: Context, private val list: ArrayList<BluetoothDevice> = ArrayList()) :
    ArrayAdapter<BluetoothDevice>(context, android.R.layout.simple_list_item_1, list) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = if (convertView == null) {
            LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1, parent, false)
        } else {
            convertView
        }

        val device = list[position]
        val label = "${device.name} (${device.address})"
        view.findViewById<TextView>(android.R.id.text1).text = label
        return view
    }
}