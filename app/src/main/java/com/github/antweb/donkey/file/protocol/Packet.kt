package com.github.antweb.donkey.file.protocol

import com.github.antweb.donkey.toHex
import java.nio.ByteBuffer
import java.util.zip.CRC32


data class Packet(val type: PacketType, val payload: ByteArray) {

    companion object {
        fun fromBytes(bytes: ByteArray): Packet {
            val type = when(bytes[0].toChar()) {
                's' -> PacketType.START
                'S' -> PacketType.START_ACK
                'c' -> PacketType.CHUNK
                'C' -> PacketType.CHUNK_ACK
                'f' -> PacketType.FINISH
                'F' -> PacketType.FINISH_ACK
                'e' -> PacketType.ERROR
                'E' -> PacketType.ERROR_ACK
                else -> throw InvalidPacketException()
            }

            val payload = bytes.slice(1 until bytes.size)

            val hex = toHex(bytes)

            return Packet(type, payload.toByteArray())
        }
    }

    fun getBytes(): ByteArray {
        val buffer = ByteBuffer.allocate(payload.size + 1)
        buffer.put(type.prefix.toByte())
        buffer.put(payload)

        return buffer.array()
    }

    fun getCrc(): Long {
        val crc = CRC32()
        crc.update(getBytes())
        return crc.value
    }
}