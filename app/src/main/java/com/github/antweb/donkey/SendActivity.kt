package com.github.antweb.donkey

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.github.antweb.donkey.file.ChunkedReader
import com.github.antweb.donkey.file.FileTransfer
import com.github.antweb.donkey.file.FileTransferListener
import java.lang.Exception
import java.lang.IllegalStateException

private const val TAG = "SendActivity"
private const val INTENT_RESULT_CODE = 1

class SendActivity : AppCompatActivity(), GattListener, FileTransferListener {

    private var isSending = false
    private var transfer: FileTransfer? = null

    private lateinit var buttonPickFile: Button
    private lateinit var buttonStartStop: Button
    private lateinit var tvSelected: TextView
    private lateinit var tvStatus: TextView
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send)

        tvSelected = findViewById(R.id.label_selected)
        tvStatus = findViewById(R.id.label_status)
        progressBar = findViewById(R.id.progress)

        buttonPickFile = findViewById(R.id.button_pick_file)
        buttonPickFile.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.type = "*/*"
            startActivityForResult(intent, INTENT_RESULT_CODE)
        }

        buttonStartStop = findViewById(R.id.button_start_stop_transfer)

        toggleControls()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode != INTENT_RESULT_CODE) {
            return
        }

        val uri = data?.data ?: return

        try {
            val reader = ChunkedReader(this, uri, ConnectionService.mtu)
            val service = ConnectionService.leService ?: throw IllegalStateException()

            transfer = FileTransfer(service, reader, this)
        } catch (e: Exception) {
            Log.e(TAG, "Failed to initialize transfer")
            return
        }

        buttonStartStop.isEnabled = true
        tvSelected.text = uri.path
    }

    private fun toggleControls() {
        if (isSending) {
            progressBar.visibility = View.VISIBLE
            buttonPickFile.isEnabled = false
            buttonStartStop.text = getString(R.string.send_button_stop_transfer)

            buttonStartStop.setOnClickListener {
                transfer?.abort()
                transfer = null
                isSending = false

                runOnUiThread {
                    toggleControls()
                }
            }
        } else {
            progressBar.visibility = View.INVISIBLE
            buttonPickFile.isEnabled = true
            buttonStartStop.text = getString(R.string.send_button_start_transfer)

            buttonStartStop.setOnClickListener {
                transfer?.start()
                isSending = true

                runOnUiThread {
                    tvStatus.text = "STARTED"
                    toggleControls()
                }
            }
        }

        buttonStartStop.isEnabled = transfer != null
    }

    override fun onError() {
        runOnUiThread {
            tvStatus.text = "ERROR"
        }
    }

    override fun onFinish() {
        isSending = false
        transfer = null

        runOnUiThread {
            tvStatus.text = "FINISHED"
            toggleControls()
        }
    }
}