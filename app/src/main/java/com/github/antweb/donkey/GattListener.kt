package com.github.antweb.donkey

import android.bluetooth.BluetoothGattCharacteristic

interface GattListener {
    fun onCharacteristicWrite(characteristic: BluetoothGattCharacteristic, status: Int) {}
    fun onCharacteristicChanged(characteristic: BluetoothGattCharacteristic) {}
    fun onConnectionStateChange(state: Int) {}
}