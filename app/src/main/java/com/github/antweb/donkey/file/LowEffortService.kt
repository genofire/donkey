package com.github.antweb.donkey.file

import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattService
import android.util.Log
import com.github.antweb.donkey.ConnectionService
import com.github.antweb.donkey.GattListener
import com.github.antweb.donkey.file.protocol.Packet
import com.github.antweb.donkey.toHex
import java.nio.charset.Charset
import java.util.*

private const val TAG = "LowEffortService"

class LowEffortService(
    service: BluetoothGattService
) : GattListener {
    private val centralTx: BluetoothGattCharacteristic
    private val centralRx: BluetoothGattCharacteristic

    private val centralTxCharacteristicUuid = UUID.fromString("01422342-2342-2342-2342-234223422342")
    private val centralRxCharacteristicUuid = UUID.fromString("02422342-2342-2342-2342-234223422342")

    private var notifyEnabled = false
    private val listeners = mutableListOf<OnPacketReceivedListener>()

    init {
        val tx = service.getCharacteristic(centralTxCharacteristicUuid)
        val rx = service.getCharacteristic(centralRxCharacteristicUuid)

        if (tx == null || rx == null) {
            throw IllegalStateException()
        }

        tx.writeType = BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE

        centralTx = tx
        centralRx = rx

        ConnectionService.addGattListener(this)
    }

    fun enableNotify(gatt: BluetoothGatt) {
        if (notifyEnabled) {
            return
        }

        val notifySuccess = gatt.setCharacteristicNotification(centralRx, true)
        if (!notifySuccess) {
            Log.e(TAG, "Notify enable failed")
        } else {
            notifyEnabled = true
        }
    }

    fun sendPacket(packet: Packet): Boolean {
        Thread.sleep(100)

        Log.d(TAG, "Sending packet ${packet.type.prefix} ${toHex(packet.payload)}")
        Log.d(TAG, "Sending packet ${packet.type.prefix} ${String(packet.payload, Charset.defaultCharset())}")

        val bytes = packet.getBytes()

        centralTx.setValue(bytes)
        val status = ConnectionService.writeCharacteristic(centralTx)

        if (!status) {
            Log.d(TAG, "Write status: $status")
        }

        return status
    }

    fun addOnPacketReceivedListener(listener: OnPacketReceivedListener) {
        listeners.add(listener)
    }

    // GattListener methods
    override fun onCharacteristicWrite(characteristic: BluetoothGattCharacteristic, status: Int) {
        Log.d(TAG, "onCharacteristicWrite: $status")
    }

    override fun onCharacteristicChanged(characteristic: BluetoothGattCharacteristic) {
        listeners.map { it.onPacketReceived(Packet.fromBytes(characteristic.value)) }
    }
}