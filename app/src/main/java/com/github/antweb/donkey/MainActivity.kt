package com.github.antweb.donkey

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

private const val TAG = "MainActivity"
private const val REQUEST_CODE_PERMISSIONS = 1

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val locationPermission =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
        val storagePermission =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)

        if (locationPermission != PackageManager.PERMISSION_GRANTED ||
            storagePermission != PackageManager.PERMISSION_GRANTED
        ) {
            showPermissionView()
        } else if (ConnectionService.hasDevice()) {
            showConnectedView()
        } else {
            showNotConnectedView()
        }
    }

    private fun showPermissionView() {
        setContentView(R.layout.activity_main_no_permissions)

        val buttonRequest = findViewById<Button>(R.id.button_request_permissions)
        buttonRequest.setOnClickListener {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE),
                REQUEST_CODE_PERMISSIONS
            )
        }
    }

    private fun showConnectedView() {
        setContentView(R.layout.activity_main_connected)

        val buttonSend = findViewById<Button>(R.id.button_send)
        buttonSend.setOnClickListener {
            val intent = Intent(this, SendActivity::class.java)
            startActivity(intent)
        }

        val tvStatus = findViewById<TextView>(R.id.label_status)
        tvStatus.text =
            getString(R.string.main_label_status, ConnectionService.deviceName, ConnectionService.deviceAddress)
    }

    private fun showNotConnectedView() {
        setContentView(R.layout.activity_main_not_connected)

        val buttonConnect = findViewById<Button>(R.id.button_connect)
        buttonConnect.setOnClickListener {
            val intent = Intent(this, ScanActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        // The lazy way out...
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}
