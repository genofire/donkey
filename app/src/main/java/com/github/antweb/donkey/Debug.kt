package com.github.antweb.donkey

fun toHex(bytes: ByteArray): String {
    val sb = StringBuilder()
    for (b in bytes) {
        sb.append(String.format("%02X ", b))
    }
    return sb.toString()
}