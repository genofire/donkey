package com.github.antweb.donkey

import android.bluetooth.*
import android.content.Context
import android.util.Log
import com.github.antweb.donkey.file.LowEffortService
import java.lang.IllegalStateException
import java.lang.NullPointerException
import java.util.*

private const val TAG = "ConnectionService"

object ConnectionService {
    var device: BluetoothDevice? = null
    var leService: LowEffortService? = null
    var mtu = 100

    private var connection: BluetoothGatt? = null

    private var connectionState = BluetoothGatt.STATE_DISCONNECTED
    private var gattListeners = mutableListOf<GattListener>()

    private const val serviceUuid = "00422342-2342-2342-2342-234223422342"

    val deviceName: String?
        get() = device?.name

    val deviceAddress: String?
        get() = device?.address

    fun hasDevice(): Boolean {
        return device != null
    }

    fun isConnected() = connectionState == BluetoothGatt.STATE_CONNECTED

    fun addGattListener(listener: GattListener) {
        gattListeners.add(listener)
    }

    fun connect(context: Context) {
        if (device == null) {
            throw IllegalStateException()
        }

        // 1. Connect
        // 2. Discover services
        // 3. Change MTU
        // 4. ???
        // 5. Profit
        connection = device?.connectGatt(context, true, gattCallback, BluetoothDevice.TRANSPORT_LE)
    }

    private val gattCallback = object : BluetoothGattCallback() {
        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            if (gatt == null) {
                throw NullPointerException()
            }

            connection = gatt

            for (service in gatt.services) {
                Log.d(TAG, "Found service: ${service.uuid}")

                if (service.uuid.toString() == serviceUuid) {
                    leService = LowEffortService(service)
                }
            }

            if (leService == null) {
                Log.e(TAG, "Could not find file transfer service")
                return
            }

            gatt.requestMtu(mtu)
//            leService?.enableNotify(gatt)
        }

        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            connectionState = newState
            connection = gatt

            gattListeners.map { it.onConnectionStateChange(newState) }

            when (newState) {
                BluetoothGatt.STATE_CONNECTED -> {
                    gatt?.discoverServices()
                }
            }
        }

        override fun onMtuChanged(gatt: BluetoothGatt?, mtu: Int, status: Int) {
            Log.d(TAG, "MTU changed to: $mtu")

            if (gatt == null) {
                throw IllegalStateException()
            }

            leService?.enableNotify(gatt)
        }

        override fun onCharacteristicWrite(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            if (gatt == null || characteristic == null) {
                throw IllegalStateException()
            }

            connection = gatt

            gattListeners.map { it.onCharacteristicWrite(characteristic, status) }
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
            connection = gatt

            if (gatt == null || characteristic == null) {
                throw IllegalStateException()
            }

            gattListeners.map { it.onCharacteristicChanged(characteristic) }
        }
    }

    fun writeCharacteristic(characteristic: BluetoothGattCharacteristic): Boolean {
        return connection?.writeCharacteristic(characteristic) ?: false
    }

}